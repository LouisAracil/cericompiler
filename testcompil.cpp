//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char current,nextChar;				// Current car	

//~ void ReadChar(void){		// Read character and skip spaces until 
				//~ // non space character is read
	//~ while(cin.get(current) && (current==' '||current=='\t'||current=='\n'))
	   	//~ cin.get(current);
//~ }
void ReadChar(void){		// Read character and skip spaces until 
				// non space character is read
	current=nextChar;
	while(cin.get(nextChar) && (current==' '||current=='\t'||current=='\n')){
		current=nextChar;
	   	//cin.get(nextChar);
	}
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
void ArithmeticExpression(void);			// Called by Term() and calls Term()
void RelationalOperator (void);

void ComparaisonExpression (void){
	char opcomp;
	ArithmeticExpression();
	opcomp=current;
	RelationalOperator();
	ArithmeticExpression();
	cout<<"pop %rbx"<<"\t # resultat de l'expression 2"<<endl;
	cout<<"pop %rax"<<"\t # resultat de l'expression 1"<<endl;
	switch(opcomp){
		cout<<"\t cmpq %rax, %rbx"<<endl;
		
		case '>' : 
		if(nextChar=='='){
			cout<<"\t pop %rax"<<endl;
			cout<<"\t jae Sup101"<<endl;
			cout<<"\t Then101:"<<endl;
			cout<<"\t 0"<<endl;
			cout<<"\t jnp suite101:"<<endl;
			cout<<"\t Sup101:"<<endl;
			cout<<"\t 99999999"<<endl;
		}
		else{
			cout<<"\t pop %rax"<<endl;
			cout<<"\t ja Sup101"<<endl;
			cout<<"\t Then101:"<<endl;
			cout<<"\t 0"<<endl;
			cout<<"\t jnp suite101:"<<endl;
			cout<<"\t Sup101:"<<endl;
			cout<<"\t 99999999"<<endl;
		}
		break;
		
		case '<' : 
			if(nextChar=='='){
			cout<<"\t pop %rax"<<endl;
			cout<<"\t jbe Sup101"<<endl;
			cout<<"\t Then101:"<<endl;
			cout<<"\t 0"<<endl;
			cout<<"\t jnp suite101:"<<endl;
			cout<<"\t Sup101:"<<endl;
			cout<<"\t 99999999"<<endl;
			}
			else{
			cout<<"\t pop %rax"<<endl;
			cout<<"\t jb Sup101"<<endl;
			cout<<"\t Then101:"<<endl;
			cout<<"\t 0"<<endl;
			cout<<"\t jnp suite101:"<<endl;
			cout<<"\t Sup101:"<<endl;
			cout<<"\t 99999999"<<endl;
		}
		break;
		
		case '!':
			if(nextChar!='='){
				Error("= attendu");
			}
			else{
			cout<<"\t pop %rax"<<endl;
			cout<<"\t jne Sup101"<<endl;
			cout<<"\t Then101:"<<endl;
			cout<<"\t 0"<<endl;
			cout<<"\t jnp suite101:"<<endl;
			cout<<"\t Sup101:"<<endl;
			cout<<"\t 99999999"<<endl;
				
			}
		break;
		
		case '=':
			if(nextChar!='='){
				Error("= attendu");
			}
			else{
			cout<<"\t pop %rax"<<endl;
			cout<<"\t jne Sup101"<<endl;
			cout<<"\t Then101:"<<endl;
			cout<<"\t 0"<<endl;
			cout<<"\t jnp suite101:"<<endl;
			cout<<"\t Sup101:"<<endl;
			cout<<"\t 99999999"<<endl;
				
			}
		
	}
}



void RelationalOperator (void){
		if(current=='==' || current=='!=' || current=='<' || current=='>' || current=='<=' || current=='>='  ){
		ReadChar();
		}
		else
			Error("Operateur de comparaison attendu");
	}
	
void AdditiveOperator(void){
		if(current=='+' || current=='-'  )
			ReadChar();
	else
		Error("Opérateur additif attendu");	   // Additive operator expected

}

bool Digit(void){
	if((current<'0')||(current>'9'))
		return false;		   // Digit expected
	else{
		//cout << "\tpush $"<<current<<endl;
		ReadChar();
		return true;
	}
}
			
void Number(void){
	if((current<'0')||(current>'9')){
		Error("Chiffre attendu");
	}
	else{
		string nb;
		nb=current;
		while(Digit()){
			nb+=current; // Temporary storage
			//ReadChar();
		}
		cout << "\tpush $"<<nb<<endl;

	}
	
	
}
	


//void ArithmeticExpression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		if(current='-'|| current=='+')
			ArithmeticExpression();
		if(current=='==' || current=='!=' || current=='<' || current=='>' || current=='<=' || current=='>='  )
			ComparaisonExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9')
			Number();
	     	else
			Error("'(' ou chiffre attendu");
}



void ArithmeticExpression(void){
	char adop;
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}

}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	ReadChar();
	ArithmeticExpression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





